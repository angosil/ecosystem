import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

# Assign spreadsheet filename: file
file = '20180223 Informe de parcheado.xlsx'

# Load spreadsheet: xl
xl = pd.ExcelFile(file)

# Print sheet names
print(xl.sheet_names)

# Load a sheet into a DataFrame by name: df1
df1 = xl.parse('Fase2')

# Print the head of the DataFrame df1
print(df1.head())

# Print df1['Tipo de EC'].unique()
print(df1['Tipo de EC'].unique())

# filter by ['SO:Máquina Virtual', 'SO:Máquina Física', 'SS:Cluster']
df1 = df1.loc[df1['Tipo de EC'].isin(['SO:Máquina Virtual', 'SO:Máquina Física', 'SS:Cluster'])]

# create DF only for production environment
print(df1['entorno_name'].unique())
df1_production = df1.loc[df1['entorno_name'] == 'PRODUCCION']
df1_production_entorno_name_unique = df1_production['entorno_name'].unique()
print(df1_production_entorno_name_unique)

# delete columns


# search in APP
pd_series_aplicaciones_count = pd.Series(df1_production.groupby('Aplicaciones')['Aplicaciones'].transform('count'), index=df1_production.index)
df1_production = df1_production.assign(freq=pd_series_aplicaciones_count)
print(df1_production[['Sistema', 'Aplicaciones', 'freq']])

#
# class GroupByApp:
#
#     def grup_by_app(index, values):
#         app = values['Aplicaciones']
#         pass
#
#
# pd_series_grup_by_app = pd.Series([grup_by_app(index, values) for index, values in df1_production.iterrows()], index=df1_production.index)
#

