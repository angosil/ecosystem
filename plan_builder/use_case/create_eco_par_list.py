class CreateEcoParList:

    def __init__(self, repository, configuration):
        self.repository = repository
        self.configuration = configuration

    def execute(self, os_collection=None, limit=100):
        eco_par_name_list = list()
        # listados de nombres de servidores
        server_name_list = self.repository.server_name_list(os_collection, limit)
        # mientras exixtan elementos
        while len(server_name_list) > 0:
            # utilizar el primero de la lista
            server_name = server_name_list[0]
            # busca el listado de nombres de servidores que pertenecen al ecosistema del primero de la lista
            eco_par = self.repository.eco_par_by_server_name(server_name, os_collection)
            eco_par_server_mane_list = eco_par.server_name_list()
            # guardar el ecosistema
            eco_par.name = self.repository.save(eco_par)
            # eleminar el listado de nobre del eco_par del listado de ecosistemas
            server_name_list = [
                server_name for server_name in server_name_list if server_name not in eco_par_server_mane_list
            ]
            eco_par_name_list.append(dict(name=eco_par.name, servers_count=len(eco_par_server_mane_list)))
        # buscar lista de ecosistemas retornar la lista de ecosistemas con la cantidad de servidores que tienen
        return eco_par_name_list

    @property
    def repository(self):
        return self._repository

    @repository.setter
    def repository(self, repository):
        self._repository = repository

    @property
    def configuration(self):
        return self._cofiguration

    @configuration.setter
    def configuration(self, configuration):
        self._cofiguration = configuration
