from plan_builder.entity.eco_par import EcoPar
from plan_builder.infrastructure.processor.neo4j_processor import Neo4jProcessor


class Neo4jRepository:
    def __init__(self, processor: Neo4jProcessor):
        self.processor = processor
        pass

    def server_name_list(self, os_collection=None, limit=100):
        server_list = self.processor.servers_name_by_os_list(os_collection, limit)
        server_name_list = list()
        for server_name in server_list:
            server_name_list.append(server_name["server_name"])
        return server_name_list

    def eco_par_by_server_name(self, server_name, os_collection=None):
        eco_data = self.processor.eco_par_by_server(os_collection, server_name, 200)
        eco_par = EcoPar(eco_data)
        return eco_par

    def save(self, eco_par: EcoPar):
        if eco_par.name:
            return eco_par.name
        server_name_list = eco_par.server_name_list()
        eco_par_name = self.processor.search_eco_par_name(server_name_list)
        if len(eco_par_name) > 1:
            server_name_string = ', '.join(server_name_list)
            raise Exception('more than one eco_par in the list of server names: {0}'.format(server_name_string))
        elif len(eco_par_name) == 1:
            return eco_par_name[0]['eco_par_name']
        eco_par_name = self.processor.save_eco_par(server_name_list)
        return eco_par_name[0]['eco_par_name']

    @property
    def processor(self):
        return self._processor

    @processor.setter
    def processor(self, processor):
        self._processor = processor
