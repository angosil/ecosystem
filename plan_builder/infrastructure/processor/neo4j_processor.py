from plan_builder.infrastructure.connector.neo4j_connector import Neo4jConnector


class Neo4jProcessor:

    def __init__(self, neo4j_connection: Neo4jConnector.connection):
        """
        init instance
        :param neo4j_connection: Neo4j started connection
        """
        self._connection = neo4j_connection
        self.server_list = list()

    @property
    def server_list(self):
        server_list = self._server_list
        if len(server_list) == 1:
            server_list = server_list[0]
        return server_list

    @server_list.setter
    def server_list(self, server_list):
        self._server_list = server_list

    def servers_by_os_list(self, os, limit=100,  whit_eco_par=0):
        """
        Get server list filter by operate system and ECO_PAR
        :param limit: int
        :param os: list
        :param whit_eco_par: [0, 1, 2]
        :return: list or dict
        """
        server_list = self._connection.run(
            '''
            MATCH (os:OS)-[:INSTALLED]->(serv)<-[:RUN_IN]-(app)
            WHERE os.Name IN $server_list
            RETURN 
                DISTINCT serv.Name AS server_name,
                serv.Environment AS server_environment,
                os.Name AS os_name, 
                os.Version AS os_version, 
                count(app) AS count_app, 
                collect(app.Name) AS apps
            ORDER BY count_app DESC
            LIMIT $limit
            ''',
            parameters={
                'server_list': os,
                'limit': limit
            }
        ).data()
        return server_list if len(server_list) > 1 else server_list[0]

    def eco_par_by_server(self, os, server, limit=100):
        """
        build or find the ECO_PAR of a server
        :param os: list
        :param server: str
        :param limit: int
        :return: void
        """

        query = '''
        MATCH (head_serv:Server{Name: $server})
        MATCH (head_serv)-[:RUN_IN*1..11]-(app)
        WITH DISTINCT app
        MATCH (app)-[:RUN_IN]->(serv_2:Server)<-[:INSTALLED]-(os:OS)
        '''
        query = self.set_os_filter_to_query(os, query)
        query += '''
        RETURN DISTINCT serv_2.Name AS server_name, 
        serv_2.Environment AS server_environment,
        CASE serv_2.Environment
            WHEN "TEST" THEN 0
            WHEN "PRE-PRODUCCION" THEN 1
            WHEN "PRODUCCION" THEN 2
            ELSE -1
        END AS env_value, 
        collect(DISTINCT app.Name) AS apps, 
        count(DISTINCT app.Name) AS count_apps, 
        collect(DISTINCT os.Name) AS os_name
        ORDER BY env_value 
        '''
        query = self.set_limit_to_query(limit, query)

        server_list = self._connection.run(
            query,
            parameters={
                'server_list': os,
                'server': server,
                'limit': limit
            }
        ).data()
        return server_list

    def servers_name_by_os_list(self, os=None, limit=100,  whit_eco_par=0):
        """
        Get server name list filter by operate system and ECO_PAR
        :param limit: int
        :param os: list
        :param whit_eco_par: [0, 1, 2]
        :return: list or dict
        """

        query = '''
        MATCH (os:OS)-[:INSTALLED]->(serv:Server)<-[:RUN_IN]-(app)
        '''
        query = self.set_os_filter_to_query(os, query)
        query += '''
        WITH serv.Name AS server_name, count(app) AS count_app
        ORDER BY count_app DESC
        RETURN DISTINCT server_name
        '''
        query = self.set_limit_to_query(limit, query)

        self.server_list = self._connection.run(
            query,
            parameters={
                'server_list': os,
                'limit': limit
            }
        ).data()
        return self.server_list

    def search_eco_par_name(self, server_name_list):
        query = '''
        // find eco_par
        MATCH (serv:Server)<-[:BELONG]->(eco)
        WHERE serv.Name IN $server_list
        RETURN DISTINCT eco.Name AS eco_par_name
        '''

        eco_par_name = self._connection.run(
            query,
            parameters={
                'server_list': server_name_list,
            }
        ).data()
        return eco_par_name

    def save_eco_par(self, server_name_list):
        query = '''
        // get unique id
        MERGE (id:UniqueId{Name:'EcoPar'})
        ON CREATE SET id.count = 1
        ON MATCH SET id.count = id.count + 1
        WITH id.count AS uid
        // create Person node
        MATCH (serv:Server)
        WHERE serv.Name IN $server_list
        MERGE (eco:EcoPar{id:uid, Name:'eco_par_'+uid})
        MERGE (serv)<-[:BELONG]->(eco)
        RETURN DISTINCT eco.Name AS eco_par_name
        '''

        eco_par_name = self._connection.run(
            query,
            parameters={
                'server_list': server_name_list,
            }
        ).data()
        return eco_par_name

    @staticmethod
    def set_os_filter_to_query(os, query):
        if os:
            query += '''
            WHERE os.Name IN $server_list
            '''
        return query

    @staticmethod
    def set_limit_to_query(limit, query):
        if limit:
            query += '''
            LIMIT $limit
            '''
        return query
