from mysql import connector

from plan_builder.infrastructure.connector.generic_connector import Connector


class MysqlConnector(Connector):
    connection = ''

    def __init__(self, configuration):
        super().__init__(configuration.mysql_host, configuration.mysql_port, configuration.mysql_db,
                         configuration.mysql_user, configuration.mysql_password)

    def connect(self):
        try:
            self.connection = connector \
                .connect(host=self.host, port=self.port, database=self.database, user=self.user, password=self.password)
        except Exception as exp:
            return exp
        return self.connection

    def close(self):
        self.connection.close()
