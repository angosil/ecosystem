from py2neo import Graph

from plan_builder.infrastructure.connector.generic_connector import Connector


class Neo4jConnector(Connector):
    connection = ''

    def __init__(self, configuration):
        super().__init__(configuration.neo4j_host, configuration.neo4j_port, configuration.neo4j_db,
                         configuration.neo4j_user, configuration.neo4j_password)

    def connect(self):
        try:
            uri = "http://{}:{}/db/{}/".format(self.host, str(self.port), self.database)
            self.connection = Graph(uri, user=self.user, password=self.password)
        except Exception as exp:
            return exp
        return self.connection

    def close(self):
        self.connection.close()
