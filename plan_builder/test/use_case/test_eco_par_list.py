import unittest

from plan_builder.config.config import Develop as configuration
from plan_builder.infrastructure.connector.neo4j_connector import Neo4jConnector
from plan_builder.infrastructure.processor.neo4j_processor import Neo4jProcessor
from plan_builder.infrastructure.repository.neo4j_repository import Neo4jRepository
from plan_builder.use_case.create_eco_par_list import CreateEcoParList


class TestEcoParList(unittest.TestCase):
    def setUp(self):
        connector = Neo4jConnector(configuration)
        connector.connect()
        processor = Neo4jProcessor(connector.connection)
        repository = Neo4jRepository(processor)
        self._eco_par_list = CreateEcoParList(repository, configuration)

    def tearDown(self):
        pass

    def test_execute_pass(self):
        windows_servers = configuration.os_collection['RED_HAT']
        eco_par_list = self._eco_par_list.execute(windows_servers, limit=10)
        eco_par_list
