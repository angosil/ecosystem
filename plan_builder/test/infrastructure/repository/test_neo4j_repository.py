import unittest

from plan_builder.config.config import Test as configuration
from plan_builder.entity.eco_par import EcoPar
from plan_builder.infrastructure.connector.neo4j_connector import Neo4jConnector
from plan_builder.infrastructure.processor.neo4j_processor import Neo4jProcessor
from plan_builder.infrastructure.repository.neo4j_repository import Neo4jRepository
from plan_builder.test.data_fixtures.data_fixtures import *


class TestNeo4jRepository(unittest.TestCase):
    def setUp(self):
        connector = Neo4jConnector(configuration)
        connector.connect()
        neo4j_processor = Neo4jProcessor(connector.connection)
        # neo4j_processor = MockNeo4jProcessor()
        self._neo4j_repository = Neo4jRepository(neo4j_processor)
        pass

    def tearDown(self):
        pass

    def test_save_pass(self):
        no_name_eco_par = EcoPar(no_name_eco_par_data)
        eco_par_name = self._neo4j_repository.save(no_name_eco_par)
        self.assertEqual(eco_par_name, 'Eco_Par_1')


class MockNeo4jProcessor(Neo4jProcessor):
    def servers_name_by_os_list(self, os=None, limit=100, whit_eco_par=0):
        return list(
            dict(eco_par_name='Eco_Par_1')
        )
