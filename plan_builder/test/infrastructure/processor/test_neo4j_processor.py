import unittest
from plan_builder.config.config import Test as configuration
from plan_builder.infrastructure.connector.neo4j_connector import Neo4jConnector
from plan_builder.infrastructure.processor.neo4j_processor import Neo4jProcessor


class TestNeo4jProcessor(unittest.TestCase):
    def setUp(self):
        connector = Neo4jConnector(configuration)
        connector.connect()
        self._neo4j_processor = Neo4jProcessor(connector.connection)

    def test_servers_name_by_os_list(self):
        servers_name = self._neo4j_processor.servers_name_by_os_list(os=None)
        self.assertTrue(True)


class MockNeo4jConnector(Neo4jConnector):
    class Connector:
        class Connection:
            def data(self):
                return list
        connection = True

        def run(self, query='', parameters=dict):
            return list()

    connector = Connector.connection
