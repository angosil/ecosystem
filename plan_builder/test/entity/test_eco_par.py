import unittest

from plan_builder.entity.eco_par import EcoPar

from plan_builder.test.data_fixtures.data_fixtures import *


class TestEcoPar(unittest.TestCase):

    def setUp(self):
        self._no_name_eco_par = EcoPar(no_name_eco_par_data)

    def tearDown(self):
        pass

    def test_serialize_no_name(self):
        no_name_eco_par_serialization = self._no_name_eco_par.serialize()
        self.assertIn('"name": null', no_name_eco_par_serialization)
