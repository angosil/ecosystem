no_name_eco_par_data = [{'server_name': 'V12KSHWED',
  'server_environment': 'TEST',
  'env_value': 0,
  'apps': ['AP-CONFOFI-DSL',
   'AP-PTGESDISTEMERGENCIA-DSL',
   'AP-HOMOLOGACIONES HW MICRO-DSL'],
  'count_apps': 3,
  'os_name': ['WINDOWS SERVER 2012 R2 STANDARD']},
 {'server_name': 'V12KROWED',
  'server_environment': 'TEST',
  'env_value': 0,
  'apps': ['AP-CONFOFI-DSL',
   'AP-PTGESDISTEMERGENCIA-DSL',
   'AP-HOMOLOGACIONES HW MICRO-DSL'],
  'count_apps': 3,
  'os_name': ['WINDOWS SERVER 2012 R2 STANDARD']},
 {'server_name': 'V12KROWET',
  'server_environment': 'PRE-PRODUCCION',
  'env_value': 1,
  'apps': ['AP-CONFOFI-DSL',
   'AP-PTGESDISTEMERGENCIA-DSL',
   'AP-HOMOLOGACIONES HW MICRO-DSL'],
  'count_apps': 3,
  'os_name': ['WINDOWS SERVER 2012 R2 STANDARD']},
 {'server_name': 'V12KSHWET',
  'server_environment': 'PRE-PRODUCCION',
  'env_value': 1,
  'apps': ['AP-CONFOFI-DSL',
   'AP-PTGESDISTEMERGENCIA-DSL',
   'AP-HOMOLOGACIONES HW MICRO-DSL'],
  'count_apps': 3,
  'os_name': ['WINDOWS SERVER 2012 R2 STANDARD']},
 {'server_name': 'V12KROWEP',
  'server_environment': 'PRODUCCION',
  'env_value': 2,
  'apps': ['AP-CONFOFI-DSL',
   'AP-PTGESDISTEMERGENCIA-DSL',
   'AP-HOMOLOGACIONES HW MICRO-DSL'],
  'count_apps': 3,
  'os_name': ['WINDOWS SERVER 2012 R2 STANDARD']},
 {'server_name': 'V12KSHWEP',
  'server_environment': 'PRODUCCION',
  'env_value': 2,
  'apps': ['AP-CONFOFI-DSL',
   'AP-PTGESDISTEMERGENCIA-DSL',
   'AP-HOMOLOGACIONES HW MICRO-DSL'],
  'count_apps': 3,
  'os_name': ['WINDOWS SERVER 2012 R2 STANDARD']}]