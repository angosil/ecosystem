import json

from plan_builder.entity.server import Server


class EcoPar:
    def __init__(self, server_list=list(), name=None):
        self.name = name
        self._servers = list()
        apps = set()
        for server_data in server_list:
            apps = apps | set(server_data['apps'])
            server = Server(
                host_name=server_data['server_name'],
                host_environment=server_data['server_environment'],
                apps=server_data['apps']
            )
            self._servers.append(server)
        self._apps = list(apps)

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    @property
    def apps(self):
        """

        :return: list
        """
        return self._apps

    @property
    def servers(self):
        """

        :return: list
        """
        return self._servers

    def add_server(self, server: Server):
        """

        :param server: Server
        :return: void
        """
        self._servers.append(server)

    def normalize(self):
        """
        Return a dictionary normalisation of EcoPar object instance
        :return: dict
        """
        data = []
        for server in self.servers:
            data.append(server.normalize())
        return dict(name=self.name, apps=self.apps, servers=data)

    def serialize(self):
        """
        Return a serialisation of EcoPar object instance
        :return: str
        """
        return json.dumps(self.normalize())

    def server_name_list(self):
        server_name_list = list()
        for server in self.servers:
            server_name_list.append(server.host_name)
        return server_name_list
