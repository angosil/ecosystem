import json


class Server:
    def __init__(self, host_name, host_environment, apps):
        """

        :param host_name:
        :param host_environment:
        :param apps:
        """
        self.host_name = host_name
        self.host_environment = host_environment
        self.apps = apps

    @property
    def host_name(self):
        """
        Get the host name of the server
        :return: str
        """
        return self._host_name

    @host_name.setter
    def host_name(self, host_name):
        """
        Set the property host name
        :param host_name: str
        :return: void
        """
        self._host_name = host_name

    @property
    def host_environment(self):
        """
        Get the environment of the server
        :return: str
        """
        return self._host_environment

    @host_environment.setter
    def host_environment(self, host_environment):
        """
        Set the environment of the server
        :param host_environment:
        :return: void
        """
        self._host_environment = host_environment

    @property
    def apps(self):
        return self._apps

    @apps.setter
    def apps(self, apps):
        self._apps = apps

    def normalize(self):
        """
        Return a dictionary representation of Server object instance
        :return:
        """
        return dict(
            host_name=self.host_name,
            host_environment=self.host_environment,
            apps=self.apps
        )

    def serialize(self):
        """
        Return a string serialisation of Server object instance
        :return: str
        """
        return json.dumps(
            self.normalize()
        )
